FROM openjdk:8-jdk
RUN apt-get --quiet update --yes && \
    apt-get --quiet install --yes python3 python3-lxml
